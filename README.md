# Docker Image: MRTG

## General Description

Run MRTG in docker container

## Volumes to be mounted

* /var/log/mrtg - log files
* /srv/mrtg - graphs and other data
* /etc/mrtg/conf.d - include files containing target information

## cfgmaker and indexmaker
Use cfgmaker and indexmaker to get the target related configuration which can be added to /etc/mrtg/conf.d

cfgmaker --global 'WorkDir: /srv/mrtg' --output /etc/mrtg/mrtg.cfg COMMUNITY@ROUTER_IP
delete unneeded interfaces from /etc/mrtg/mrtg.cfg

indexmaker --output=/srv/mrtg/index.html /etc/mrtg/mrtg.cfg