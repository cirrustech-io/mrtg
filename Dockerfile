FROM phusion/baseimage

MAINTAINER Ali Khalil <ali.khalil@gmail.com>
EXPOSE 80
CMD ["/sbin/my_init"]
ENV LANG C

# Update existing packages and install new ones
# But, first add GPG key for Nginx PPA repository
RUN apt-get -q update && \
  DEBIAN_FRONTEND=noninteractive apt-get install -qy \
    mrtg \
    nginx \
    snmpd \
    && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /var/cache/apt/*.bin /tmp/* /etc/cron.d/mrtg

# Copy all needed files to image
COPY ./fs/ /
