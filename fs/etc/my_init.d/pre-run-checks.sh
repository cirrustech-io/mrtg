#! /bin/bash
#
# Check if applications exist and download them if necessary
#

# Exit if any errors encountered
set -e

# Mount Point for LANcache data
MOUNT="/srv/mrtg"

if [ ! -d $MOUNT ]
then
	echo "ERROR: $MOUNT - Does not exist."
	exit 1
else
	echo "OK: $MOUNT - exists"
fi

if [[ ! -d "/var/log/mrtg" ]]; then
  echo "Log mount location does not exist"
  exit 1
else
  echo "Log mount location: OK"
fi

#Set up Nginx site and disable default site 
ln -s /etc/nginx/sites-available/mrtg.conf /etc/nginx/sites-enabled/mrtg.conf
rm /etc/nginx/sites-enabled/default

